
/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik St{rfeldt, Tom Madsen, and Katja Nyboe.   *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
*       ROM 2.4 is copyright 1993-1996 Russ Taylor                         *
*       ROM has been brought to you by the ROM consortium                  *
*           Russ Taylor (rtaylor@efn.org)                                  *
*           Gabrielle Taylor                                               *
*           Brian Moore (zump@rom.org)                                     *
*       By using this code, you have agreed to follow the terms of the     *
*       ROM license, in the file Rom24/doc/rom.license                     *
***************************************************************************/

#if defined(macintosh)
#include <types.h>
#else
#include <sys/types.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "merc.h"

extern char * const dir_name[];
int chance;
void scan_list           args((ROOM_INDEX_DATA *scan_room, CHAR_DATA *ch,
                               sh_int depth, sh_int door));

void scan_char           args((CHAR_DATA *victim, CHAR_DATA *ch));

void do_scan_range       args(( CHAR_DATA *ch, char *argument, int add ));

void do_scan ( CHAR_DATA *ch, char *argument )
{
 do_scan_range( ch, argument , 0 );
 return;
}
/*
void do_far_sight ( CHAR_DATA *ch, char *argument )
{
if ( ( chance = get_skill ( ch, gsn_farsight ) ) == 0
        || (!IS_NPC(ch) && ch -> level < skill_table[gsn_farsight].skill_level[ch->class] ) )
{
        send_to_char("Huh?\n\r",ch);
        return;
}
 if ( number_percent ( ) > chance )
   {
        send_to_char ("You try see distant land, but fail!\n\r", ch);
         do_scan_range( ch, argument, 0 );
        check_improve(ch, gsn_farsight, FALSE, 1);
         return;
   }
 do_scan_range( ch, argument , 2 );
 check_improve(ch, gsn_farsight, TRUE, 1);
  return;
}
*/
void do_scan_range( CHAR_DATA *ch, char *argument, int add )
{
    CHAR_DATA           *fch;
    CHAR_DATA           *fch_next;
    ROOM_INDEX_DATA     *in_room;
    ROOM_INDEX_DATA     *to_room;
    EXIT_DATA           *pexit;
    bool                see;
    int                 door;
    int                 i;
    int                 range;
    char                buf[MAX_STRING_LENGTH];
    char                arg1[MAX_INPUT_LENGTH];


  argument = one_argument(argument, arg1);
/*
        no_hide (ch);
        no_camouflage(ch);
*/
   if (arg1[0] == '\0')
   {
      act("$n looks all around.", ch, NULL, NULL, TO_ROOM);
      send_to_char("Looking around you see:\n\r", ch);
                scan_list(ch->in_room, ch, 0, -1);

      for (door=0;door<6;door++)
      {
         if ((pexit = ch ->in_room->exit[door]) != NULL)
         {
            sprintf(buf,"- %s -\n",dir_name[door]);
            send_to_char(buf,ch);
            scan_list(pexit->u1.to_room, ch, 1, door);
         }
      }
      return;
   }

   if (!str_cmp(arg1, "n") || !str_cmp(arg1, "north")) door = 0;
   else if (!str_cmp(arg1, "e") || !str_cmp(arg1, "east"))  door = 1;
   else if (!str_cmp(arg1, "s") || !str_cmp(arg1, "south")) door = 2;
   else if (!str_cmp(arg1, "w") || !str_cmp(arg1, "west"))  door = 3;
   else if (!str_cmp(arg1, "u") || !str_cmp(arg1, "up" ))   door = 4;
   else if (!str_cmp(arg1, "d") || !str_cmp(arg1, "down"))  door = 5;
   else { send_to_char("Which way do you want to scan?\n\r", ch); return; }

    if ( door == -1 )
    {
        send_to_char( "I've never heard of such a direction!\n\r", ch );
        return;
    }

    if ( IS_AFFECTED(ch,AFF_BLIND) )
    {
        send_to_char( "You can't see a thing!\n\r", ch );
        return;
    }
    if ( add !=0 ) {
        act( "You far scan $T.", ch, NULL, dir_name[door], TO_CHAR );
        act( "$n far scans $T.", ch, NULL, dir_name[door], TO_ROOM );
                }
        else {
    act( "You scan $T.", ch, NULL, dir_name[door], TO_CHAR );
    act( "$n scans $T.", ch, NULL, dir_name[door], TO_ROOM );
             }
    range = ch->level/15;
    if ( ( weather_info.sky == SKY_RAINING ||
/*           weather_info.sky == SKY_SNOWING ||	*/
           weather_info.sky == SKY_LIGHTNING ) && IS_OUTSIDE(ch) )
    {
        send_to_char( "You peer through the rain...\n\r", ch );
        range /= 2;
    }

        range+=add;

    in_room = ch->in_room;
    for (i=0;i<range+1;i++)
    {
        if ( ( pexit   = in_room->exit[door] ) == NULL
            ||   ( to_room = pexit->u1.to_room   ) == NULL
            ||   !can_see_room(ch,pexit->u1.to_room)
            ||   IS_SET(pexit->exit_info,EX_CLOSED)
            ||   room_is_private( to_room )
            ) break;

          see = TRUE;

        if ( !room_is_dark(to_room) || i <= ch->in_room->light )
        {
            for ( fch = to_room->people; fch != NULL; fch = fch_next )
            {
                fch_next = fch->next_in_room;
                if ( can_see( ch, fch ) )
                    {
                    if ( see )
                    {
                                sprintf(buf,"\n\r --- >[Range %d]< --- \n\r",i+1);
                                send_to_char( buf, ch );
                                sprintf(buf,"%s:\n\r\n\r",to_room->name);
                                send_to_char( buf, ch );
                                see = FALSE;
                    }
                    scan_char(fch,ch);
                }
            }
        }
        else
            send_to_char( "A total darkness.\n\r", ch );
        in_room=to_room;
    }
    return;
}

void scan_list(ROOM_INDEX_DATA *scan_room, CHAR_DATA *ch, sh_int depth,
               sh_int door)
{
   CHAR_DATA *rch;

   if (scan_room == NULL) return;
   for (rch=scan_room->people; rch != NULL; rch=rch->next_in_room)
   {
      if (rch == ch) continue;
      if (!IS_NPC(rch) && rch->invis_level > get_trust(ch)) continue;
      if (can_see(ch, rch)) scan_char(rch, ch);
   }
   return;
}

void scan_char(CHAR_DATA *victim, CHAR_DATA *ch)
{

   char buf[MAX_INPUT_LENGTH];

   if ( !IS_NPC(victim) )
        sprintf(buf,"%s%s\n\r",victim->name,victim->pcdata->title);
   else
        sprintf(buf,"%s",victim->long_descr);

   send_to_char(buf, ch);
   return;
}

