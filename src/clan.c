/*************************************************************************/
/*                                                                       */
/*                                                                       */
/*************************************************************************/

#if defined(macintosh)
#include <types.h>
#else
#include <sys/types.h>
#endif
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "merc.h"
#include "tables.h"
#include "interp.h"

char buf[MAX_STRING_LENGTH];

void do_clanpractice( CHAR_DATA *ch, char *argument )
{
    if ( IS_NPC(ch) )
	return;
    if ( ch->clan <= 1 )
    {
    	send_to_char("Huh?\r\n", ch );
    	return;
    }

    if ( !ch->in_room->clan )
    {
	send_to_char( "You can't do that here.\n\r", ch );
	return;
    }

    if ( clan_table[ch->clan].skill[0] )
	do_practice( ch, clan_table[ch->clan].skill);

    return;
}


void do_banzay( CHAR_DATA *ch, char *argument )
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    int chance;

    one_argument(argument,arg);
 
    if ( (chance = get_skill(ch,gsn_banzay)) == 0
    ||	 (IS_NPC(ch) )
    ||	 (!IS_NPC(ch) && ch->clan != 3 ) /* Samuray */ )
    {	
	send_to_char("Banzay? What's that?\r\n",ch);
	return;
    }
 
    if (arg[0] == '\0')
    {
	victim = ch->fighting;
	if (victim == NULL)
	{
	    send_to_char("But you aren't fighting anyone!\r\n",ch);
	    return;
	}
    }

    else if ((victim = get_char_room(ch,arg)) == NULL)
    {
	send_to_char("They aren't here.\r\n",ch);
	return;
    }

    if (victim->position < POS_FIGHTING)
    {
	act("You'll have to let $M get back up first.",ch,NULL,victim,TO_CHAR);
	return;
    } 

    if (victim == ch)
    {
	send_to_char("You try to hit your brains out, but fail.\r\n",ch);
	return;
    }

    if (is_safe(ch,victim))
	return;

    if ( IS_NPC(victim) && 
	victim->fighting != NULL && 
	!is_same_group(ch,victim->fighting))
    {
        send_to_char("Kill stealing is not permitted.\r\n",ch);
        return;
    }

    if (IS_AFFECTED(ch,AFF_CHARM) && ch->master == victim)
    {
	act("But $N is your friend!",ch,NULL,victim,TO_CHAR);
	return;
    }

    if ( number_percent() < get_skill(ch,gsn_banzay))
    {
	act("$n ���� {Y'BANZAY!!!{x' � ������� � ��������� ��������!",
    		ch,NULL,NULL,TO_ROOM);
	act("�� ������� {Y'BANZAY!!!{x' � �������� � ��������� �������� !",
    		ch,NULL,NULL,TO_CHAR);
	multi_hit( ch, victim, TYPE_UNDEFINED );
	WAIT_STATE(ch,skill_table[gsn_banzay].beats);
    }
    else
    {
	act("$n ���� {Y'BANZAY!!!{x'",
    		ch,NULL,NULL,TO_ROOM);
	act("�� ����� {Y'BANZAY!!!{x'!",
    		ch,NULL,NULL,TO_CHAR);
	WAIT_STATE(ch,skill_table[gsn_banzay].beats);
    }
}

void	do_shadow( CHAR_DATA *ch, char *argument )
{
CHAR_DATA *pet;
MOB_INDEX_DATA	*pindex;
sh_int	i;

	if ( IS_NPC(ch) )
	    return;
	if ( ch->clan != CLAN_SHADOW )
	{
	    send_to_char("Huh?\r\n", ch);
	    return;
	}

	if ( ch->pet && ch->pet->pIndexData->vnum == MOB_VNUM_SHADOW )
	{
	    pet = ch->pet;
	    ch->pet = pet->pet;
	    extract_char(pet,TRUE);
	    return;
	}
	
    	pindex = get_mob_index(MOB_VNUM_SHADOW);
    	if ( pindex == NULL )
	{
    	    bug("Fread_pet: bad vnum %d.",MOB_VNUM_SHADOW);
	    return;
	}
    	else
    	    pet = create_mobile(pindex);
/*
	send_to_char("�� ���� ������ ���� ����...\r\n", ch);
*/
	SET_BIT(pet->act, ACT_PET);
	SET_BIT(pet->affected_by, AFF_CHARM);
	pet->comm = COMM_NOTELL|COMM_NOSHOUT|COMM_NOCHANNELS;
	
	sprintf(buf,"shadow of %s\n", ch->name );

	pet->description = str_dup( buf );
	pet->short_descr = str_dup("shadow");
	pet->long_descr = str_dup(buf);
	pet->clan = ch->clan;
	pet->class = ch->class;
	pet->race = ch->race;
	pet->level = ch->level;
	pet->size = ch->size;
	pet->hit = ch->hit;
	pet->max_hit = ch->max_hit;
	pet->mana = ch->mana;
	pet->max_mana = ch->max_mana;
	pet->move = ch->move;
	pet->max_move = ch->max_move;
	pet->imm_flags = ch->imm_flags;
	pet->res_flags = ch->res_flags;
	pet->vuln_flags = ch->vuln_flags;
	pet->hitroll = ch->hitroll;
	pet->damroll = ch->damroll;
	pet->affected_by = ch->affected_by;
	pet->position = ch->position;
	pet->wimpy = ch->wimpy;
	SET_BIT(pet->act,PLR_AUTOASSIST);
	
	for (i=0;i<MAX_STATS;i++)
	    pet->perm_stat[i] = ch->perm_stat[i];

	pet->timer = ch->level;
	
	char_to_room( pet, ch->in_room );
	add_follower( pet, ch );
	pet->leader = ch;
	pet->master = ch;
	pet->pet = ch->pet;
	ch->pet = pet;

}
